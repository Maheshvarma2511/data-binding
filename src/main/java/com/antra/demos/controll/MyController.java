package com.antra.demos.controll;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.antra.demos.bean.LoginBean;
import com.antra.demos.service.MyService;

@Controller
public class MyController {
	
	@Autowired
	MyService myservice;
	
	
	
	
	@GetMapping(value="index")
	public String check(Model model) {
		LoginBean obj= new LoginBean();
		model.addAttribute("obj",obj);
		return "login";
	}
	
	@PostMapping(value="check")
	public String check1(@ModelAttribute(value="obj") LoginBean bean, Model model) {
		
		String username= bean.getUsername();
		String password= bean.getPassword();
		//List<EntityClass> list = myservice.getAll();
		
		if(myservice.getData(username, password)) {
		if(username.equals("mahesh") && password.equals("mahesh251")) {
			
			//model.addAttribute("list", list);
			return "success";
			
		}
		else {
			model.addAttribute("message","Enter correctly");
			return "login";
		}
		
		
		}else {
			
			model.addAttribute("check1", "data is not saved in the data base.");
			return "login";
		}
	}

}