package com.antra.demos.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.demos.dao.MyDao;
import com.antra.demos.entity.EntityClass;

@Service
public class MyService {
	
	@Autowired
	MyDao dao;
	
	public boolean getData(String username , String password) {
		
	
		
		EntityClass e = new EntityClass();
		e.setUsername(username);
		e.setPassword(password);
		dao.save(e);
		Optional<EntityClass> optional = dao.findById(username);
		if(optional.isPresent()) {
			return true;
		}else {
			return false;
		}
		
		
	}
	public List getAll() {
		
		List<EntityClass> lst = dao.findAll();
		 return lst;
		
	}

}
