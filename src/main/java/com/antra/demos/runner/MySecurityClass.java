package com.antra.demos.runner;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;



    @EnableWebSecurity
	public class MySecurityClass extends WebSecurityConfigurerAdapter {

		@Autowired
		DataSource dataSource;
		
		@Autowired
		BCryptPasswordEncoder encoder;
		
		@Override
		protected void configure(HttpSecurity http) throws Exception {
			
			http
				.authorizeHttpRequests()
				.antMatchers("/login")
				.hasAnyRole("PRINCI")
				.and()
				.authorizeHttpRequests()
				.antMatchers("/success")
				.authenticated()
				.anyRequest().permitAll()
				.and()
				.formLogin();
		}
		
		@Autowired
		public void configureBCreypted(AuthenticationManagerBuilder builder) throws Exception {
			
			builder.jdbcAuthentication().dataSource(dataSource).
			usersByUsernameQuery("select username, password, active from users where username = ? ").
			authoritiesByUsernameQuery("select username, authority from authorities where username = ? ").
			passwordEncoder(encoder);
		}
	}

