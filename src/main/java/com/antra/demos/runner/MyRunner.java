package com.antra.demos.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

//@Component
public class MyRunner implements CommandLineRunner {

    @Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	BCryptPasswordEncoder encoder;

	String query1 = "insert into users values(?,?,?)";
	String query2 = "insert into authorities values(?,?)";

	@Override
	public void run(String... args) throws Exception {
		
		String p1 = encoder.encode("mahesh@123");
		String p2 = encoder.encode("harish@123");
		String p3 = encoder.encode("ramesh@123");

		jdbcTemplate.update(query1, "mahesh", p1, 1);
		jdbcTemplate.update(query1, "harish", p2, 1);
		jdbcTemplate.update(query1, "ramesh", p3, 0);

		jdbcTemplate.update(query2, "ROLE_PRINCI", "mahesh");
		jdbcTemplate.update(query2, "ROLE_HOD", "harish");
		jdbcTemplate.update(query2, "ROLE_STUDENT", "ramesh");
	}
}
