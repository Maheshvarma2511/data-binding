package com.antra.demos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class Application {
	
	@Bean
	public BCryptPasswordEncoder encoder() {
		
		BCryptPasswordEncoder bcencoder = new BCryptPasswordEncoder();
		return bcencoder;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
		
	}

}
