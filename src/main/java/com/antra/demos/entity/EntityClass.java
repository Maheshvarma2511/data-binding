package com.antra.demos.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="entity")
public class EntityClass {
	
	@Id
	@Column(length=20)
	String username;
	@Column(length=20)
	String password;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "EntityClass [username=" + username + ", password=" + password + "]";
	}
	
	

}
