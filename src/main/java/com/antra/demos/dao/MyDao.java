package com.antra.demos.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.antra.demos.entity.EntityClass;

@Repository
public interface MyDao extends JpaRepository<EntityClass, String> {

}
